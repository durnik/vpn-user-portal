<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal;

use Vpn\Portal\Cfg\LogConfig;

/**
 * Write to (sys)logger on connect/disconnect events.
 */
class LogConnectionHook implements ConnectionHookInterface
{
    private Storage $storage;
    private LoggerInterface $logger;
    private LogConfig $logConfig;

    public function __construct(Storage $storage, LoggerInterface $logger, LogConfig $logConfig)
    {
        $this->storage = $storage;
        $this->logger = $logger;
        $this->logConfig = $logConfig;
    }

    public function connect(string $userId, string $profileId, string $vpnProto, string $connectionId, string $ipFour, string $ipSix, ?string $originatingIp): void
    {
        $this->logger->info(
            self::logConnect($userId, $profileId, $vpnProto, $connectionId, $ipFour, $ipSix, $originatingIp)
        );
    }

    public function disconnect(string $userId, string $profileId, string $vpnProto, string $connectionId, string $ipFour, string $ipSix, int $bytesIn, int $bytesOut): void
    {
        $this->logger->info(
            self::logDisconnect($userId, $profileId, $vpnProto, $connectionId, $ipFour, $ipSix, $bytesIn, $bytesOut)
        );
    }

    private function logConnect(string $userId, string $profileId, string $vpnProto, string $connectionId, string $ipFour, string $ipSix, ?string $originatingIp): string
    {
        if (!$this->logConfig->originatingIp() || null === $originatingIp) {
            $originatingIp = '*';
        }

        return str_replace(
            [
                '{{USER_ID}}',
                '{{PROFILE_ID}}',
                '{{VPN_PROTO}}',
                '{{CONNECTION_ID}}',
                '{{IP_FOUR}}',
                '{{IP_SIX}}',
                '{{ORIGINATING_IP}}',
                '{{AUTH_DATA}}',
            ],
            [
                $userId,
                $profileId,
                $vpnProto,
                $connectionId,
                $ipFour,
                $ipSix,
                $originatingIp,
                $this->authData($userId),
            ],
            $this->logConfig->connectLogTemplate()
        );
    }

    private function logDisconnect(string $userId, string $profileId, string $vpnProto, string $connectionId, string $ipFour, string $ipSix, int $bytesIn, int $bytesOut): string
    {
        return str_replace(
            [
                '{{USER_ID}}',
                '{{PROFILE_ID}}',
                '{{VPN_PROTO}}',
                '{{CONNECTION_ID}}',
                '{{IP_FOUR}}',
                '{{IP_SIX}}',
                '{{BYTES_IN}}',
                '{{BYTES_OUT}}',
                '{{AUTH_DATA}}',
            ],
            [
                $userId,
                $profileId,
                $vpnProto,
                $connectionId,
                $ipFour,
                $ipSix,
                (string) $bytesIn,
                (string) $bytesOut,
                $this->authData($userId),
            ],
            $this->logConfig->disconnectLogTemplate()
        );
    }

    /**
     * Obtain the Base64 URL safe encoded "auth_data" column for this user from
     * the database. It is currently used to store the originating local user
     * before HMAC'ing the user's identifier for use with "Guest Access".
     */
    private function authData(string $userId): string
    {
        if (null === $userInfo = $this->storage->userInfo($userId)) {
            // user no longer exists
            return '';
        }
        if (null === $authData = $userInfo->authData()) {
            // no auth_data for this user
            return '';
        }

        return Base64UrlSafe::encodeUnpadded($authData);
    }
}
