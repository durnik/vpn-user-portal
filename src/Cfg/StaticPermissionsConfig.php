<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Cfg;

class StaticPermissionsConfig
{
    use ConfigTrait;
    private const DEFAULT_ATTRIBUTE_NAME = 'memberOf';

    private array $configData;

    public function __construct(array $configData)
    {
        $this->configData = $configData;
    }

    public function permissionsFile(): string
    {
        return $this->requireString(
            'permissionsFile',
            sprintf('%s/config/static_permissions.json', $this->requireString('baseDir'))
        );
    }

    public function isLivePermissionSource(): bool
    {
        return $this->requireBool('isLivePermissionSource', false);
    }

    public function defaultAttributeName(): string
    {
        return $this->requireString('defaultAttributeName', self::DEFAULT_ATTRIBUTE_NAME);
    }
}
