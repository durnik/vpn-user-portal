<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal;

use Vpn\Portal\Cfg\StaticPermissionsConfig;

class StaticPermissionsSource implements PermissionSourceInterface
{
    private StaticPermissionsConfig $staticPermissionsConfig;

    public function __construct(StaticPermissionsConfig $staticPermissionsConfig)
    {
        $this->staticPermissionsConfig = $staticPermissionsConfig;
    }

    /**
     * Get current permissions for users directly from the source.
     *
     * If no permissions are available, or the user no longer exists, an empty
     * array is returned.
     *
     * @return array<string>
     */
    public function permissionsForUser(string $userId): array
    {
        $permissionsFile = $this->staticPermissionsConfig->permissionsFile();
        if (!FileIO::exists($permissionsFile)) {
            return [];
        }

        $permissionData = Json::decode(FileIO::read($permissionsFile));
        $permissionList = [];
        foreach ($permissionData as $permissionId => $userIdList) {
            if (!is_string($permissionId)) {
                continue;
            }
            if (!is_array($userIdList)) {
                continue;
            }
            if (in_array($userId, $userIdList, true)) {
                if (false === strpos($permissionId, '!')) {
                    $permissionId = sprintf('%s!%s', $this->staticPermissionsConfig->defaultAttributeName(), $permissionId);
                }
                $permissionList[] = $permissionId;
            }
        }

        return $permissionList;
    }
}
